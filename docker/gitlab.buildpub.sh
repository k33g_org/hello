#!/bin/bash

set -o allexport; source .env; set +o allexport
echo "🐋 ${IMAGE_NAME}:${IMAGE_TAG}"

docker login registry.gitlab.com -u ${GITLAB_HANDLE} -p ${GITLAB_TOKEN_ADMIN}
docker build -t ${IMAGE_NAME} . 
docker tag ${IMAGE_NAME} registry.gitlab.com/${GITLAB_PATH_REGISTRY}/${IMAGE_NAME}:${IMAGE_TAG}
docker push registry.gitlab.com/${GITLAB_PATH_REGISTRY}/${IMAGE_NAME}:${IMAGE_TAG}

echo "use registry.gitlab.com/${GITLAB_PATH_REGISTRY}/${IMAGE_NAME}:${IMAGE_TAG}"
